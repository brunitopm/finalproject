const mysql = require('mysql2');
const DBOptions = require('./DBOptions');
export const pool = mysql.createPool({
  host: DBOptions.host,
  user: DBOptions.user,
  password: DBOptions.pw,
  database: DBOptions.database,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
}).promise();

//module.exports = pool;
