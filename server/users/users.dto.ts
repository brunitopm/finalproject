export interface UsersDto {
  userId: number;
  firstname: string;
  lastname: string;
  username: string;
  password: string;
  address: string;
  phoneNumber: string;
  email: string;
  type: string;
  languageId: string;
}
