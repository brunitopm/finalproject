import { Body, Controller, Delete, Get, Param, Post, Put, Headers } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersDto } from './users.dto';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {
  }

  @Get()
  async getAllUsers(): Promise<[UsersDto]> {
    return this.usersService.getAllUsers();
  }

  @Get('/:userId')
  async getOneUser(@Param() params): Promise<UsersDto> {
    return this.usersService.getOneUser(params.userId);
  }

  @Post('/register')
  async createUser(@Body() requestBody): Promise<any> {
    console.log('arrived', requestBody);
    return await this.usersService.createUser(requestBody);
  }

  @Put()
  async updateUser(@Body() requestBody): Promise<any> {
    return this.usersService.updateUser(requestBody);
  }

  @Delete('/:userId')
  async deleteUser(@Param() params): Promise<any> {
    return await this.usersService.deleteUser(params.userId);
  }
}
