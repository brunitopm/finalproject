import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { pool } from '../DBPool';
import * as crypto from 'crypto';
import { UsersDto } from './users.dto';

@Injectable()
export class UsersService {

  async findByLogin(username: string, password: string) {
    await pool.query('SELECT EXISTS(SELECT idUser FROM tblUser WHERE dtUsername = ?) INTO @user;', [username]);
    const [result] = await pool.query('SELECT @user;', []);


    if (result[0]['@user'] === 1) {
      const [error, rows, fields] = await pool.query('SELECT * FROM tblUser WHERE dtUsername = ?;', [username]);

      if (error) {
        console.log(error);
      }

      const givenPassword = crypto.pbkdf2Sync(password, error[0].dtSalt, 10000, 100, 'sha512').toString('hex');

      if (error[0].dtPassword !== givenPassword) {
        throw new HttpException('Credentials incorrect', HttpStatus.BAD_REQUEST);
      }

      return {
        username: error[0].dtUsername,
        userId: error[0].idUser,
        languageId: error[0].fiLanguage
      };
    } else {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }
  }

  async getAllUsers(): Promise<[UsersDto]> {
    const rows = await pool.query('SELECT * FROM tblUser;', []);
    return rows[0];
  }

  async getOneUser(userId: number): Promise<UsersDto> {
    const rows = await pool.query('SELECT * FROM tblUser WHERE idUser = ?;', [userId]);
    return rows[0];
  }

  async createUser(requestBody: UsersDto): Promise<any> {
    const { firstname, lastname, username, address, phoneNumber, email, languageId } = requestBody;
    let { password } = requestBody;

    const salt = crypto.randomBytes(32).toString('hex');
    password = crypto.pbkdf2Sync(password, salt, 10000, 100, 'sha512').toString('hex');

    await pool.query('CALL sp_createUser(?,?,?,?,?,?,?,?,?,@out_message);',
      [firstname, lastname, username, password, salt, address, phoneNumber, email, languageId]);
    const result = await pool.query('SELECT @out_message;', []);

    return result[0];
  }

  async updateUser(requestBody: UsersDto): Promise<any> {
    const { userId, firstname, lastname, username, address, phoneNumber, email, languageId } = requestBody;
    let { password } = requestBody;

    const salt = await pool.query('SELECT dtSalt FROM tblUser WHERE idUser = ?;', [userId]);
    password = crypto.pbkdf2Sync(password, salt[0][0].dtSalt, 10000, 100, 'sha512').toString('hex');

    await pool.query('CALL sp_updateUser(?,?,?,?,?,?,?,?,?, @out_message);',
      [userId, firstname, lastname, username, password, address, phoneNumber, email, languageId]);
    const result = await pool.query('SELECT @out_message;', []);

    return result[0];
  }

  async deleteUser(userId: number): Promise<any> {
    await pool.query('CALL sp_deleteUser(?, @out_message);', [userId]);
    const result = await pool.query('SELECT @out_message;', []);

    return result[0];
  }


}
