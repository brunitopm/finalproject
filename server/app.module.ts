import { Module } from '@nestjs/common';
import { AngularUniversalModule } from '@nestjs/ng-universal';
import { join } from 'path';
import { AppServerModule } from '../src/main.server';
import { UsersController } from './users/users.controller';
import { MessageTypesController } from './message-types/message-types.controller';
import { MessagesController } from './messages/messages.controller';
import { MessagesService } from './messages/messages.service';
import { UsersService } from './users/users.service';
import { MessageTypesService } from './message-types/message-types.service';
import { LanguagesService } from './languages/languages.service';
import { LanguagesController } from './languages/languages.controller';
import { SubscriptionsService } from './subscriptions/subscriptions.service';
import { SubscriptionsController } from './subscriptions/subscriptions.controller';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    AngularUniversalModule.forRoot({
      bootstrap: AppServerModule,
      viewsPath: join(process.cwd(), 'dist/MessageHandler/browser')
    }),
    AuthModule,
    UsersModule
  ],
  controllers: [UsersController, MessageTypesController, MessagesController, LanguagesController, SubscriptionsController],
  providers: [MessagesService, UsersService, MessageTypesService, LanguagesService, SubscriptionsService]
})
export class AppModule {
}
