export interface ContactMailDto {
  emailAddress: string;
  message: string;
}
