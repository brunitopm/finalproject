import { Injectable } from '@nestjs/common';
import { pool } from '../DBPool';
import * as JWT from 'jwt-decode';
import * as fs from 'fs';
import * as sgMail from '@sendgrid/mail';

@Injectable()
export class MessagesService {

  // get all messages of the users current language
  async getAllMessages(token: any) {
    token = token.slice(7);
    token = JWT(token);
    // tslint:disable-next-line:max-line-length
    const [rows, fields] = await pool.query('SELECT idMessage, dtParameter, dtParameterCount, tblMessage.fiMessageType, dtTypeName, dtShownTypeName, tblTranslate.fiLanguage, dtTitle, dtShownTitle, dtText FROM tblMessage, tblTranslate, tblMessageType, tblTranslateMessageType WHERE idMessage = fiMessage AND idMessageType = tblMessage.fiMessageType AND idMessageType = tblTranslateMessageType.fiMessageType AND tblTranslateMessageType.fiLanguage = ? AND tblTranslate.fiLanguage = ?;', [token.languageId, token.languageId]);
    return rows;
  }

  async getAllMessagesOnly() {
    // tslint:disable-next-line:max-line-length
    const [rows, fields] = await pool.query('SELECT * FROM tblMessage;', []);
    return rows;
  }

  // get one message of the users current language
  async getOneMessage(token: any, messageId: number) {
    token = token.slice(7);
    token = JWT(token);
    // tslint:disable-next-line:max-line-length
    const [rows, fields] = await pool.query('SELECT idMessage, dtParameter, dtParameterCount, tblMessage.fiMessageType, dtTypeName, dtShownTypeName, tblTranslate.fiLanguage, dtTitle, dtShownTitle, dtText FROM tblMessage, tblTranslate, tblMessageType, tblTranslateMessageType WHERE idMessage = ? AND idMessage = fiMessage AND idMessageType = tblTranslateMessageType.fiMessageType AND tblTranslateMessageType.fiLanguage = ? AND tblTranslate.fiLanguage = ?;', [messageId, token.languageId, token.languageId]);

    return rows[0];
  }

  // get one message of the users current language
  async getOneMessageAllLanguages(token: any, messageId: number) {
    token = token.slice(7);
    token = JWT(token);
    // tslint:disable-next-line:max-line-length
    const [rows, fields] = await pool.query('SELECT idMessage, dtParameter, dtParameterCount, fiMessageType, dtTypeName, fiLanguage, dtTitle, dtShownTitle, dtText FROM tblMessage, tblTranslate, tblMessageType WHERE idMessage = ? AND idMessage = fiMessage;', [messageId]);

    return rows[0];
  }

  // get all messages of a certain language
  async getAllMessagesOfLanguage(token: any, languageID: string) {
    token = token.slice(7);
    token = JWT(token);
    // tslint:disable-next-line:max-line-length
    const [rows, fields] = await pool.query('SELECT idMessage, dtParameter, dtParameterCount, fiMessageType, dtTypeName, fiLanguage, dtTitle, dtShownTitle, dtText FROM tblMessage, tblTranslate, tblMessageType WHERE idMessage = fiMessage AND fiMessageType = idMessageType AND fiLanguage = ?;', [languageID]);

    return rows;
  }

  // get one message of a certain language
  async getOneMessageOfLanguage(token: any, messageId: number, languageId: string) {
    token = token.slice(7);
    token = JWT(token);
    console.log(languageId);
    // tslint:disable-next-line:max-line-length
    const [rows, fields] = await pool.query('SELECT idMessage, dtParameter, dtParameterCount, fiMessageType, dtTypeName, fiLanguage, dtTitle, dtShownTitle, dtText FROM tblMessage, tblTranslate, tblMessageType WHERE idMessage = ? AND idMessage = fiMessage AND idMessageType = fiMessageType AND fiLanguage = ?;', [messageId, languageId]);

    return rows[0];
  }

  async createMessage(token: any, requestBody: any) {
    token = token.slice(7);
    token = JWT(token);

    const { shownTitle, title, parameter, typeId, text, languageId } = requestBody;

    // tslint:disable-next-line:max-line-length
    await pool.query('CALL sp_createMessage(?,?,?,?,?,?,?, @out_message);', [token.userId, typeId, text, parameter, title, shownTitle, languageId]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    return rows[0];
  }

  async sendMessage(token: any, requestBody: any) {
    token = token.slice(7);
    token = JWT(token);

    const { messageId, languageId, parameters } = requestBody;
    console.log(messageId);
    console.log(languageId);

    await pool.query('CALL sp_sendMessage(?,?,?,?, @out_message);', [messageId, token.userId, languageId, parameters]);
    const [rows, fields] = await pool.query('SELECT dtMessage FROM Messages WHERE id = LAST_INSERT_ID();', []);
    const message = rows[0].dtMessage;
    console.log(message);

    if (message !== 'Either you have to many parameters or you are missing some.') {
      console.log('here');
      const [typeName] = await pool.query('SELECT dtShownTypeName,idMessageType FROM tblMessage, tblMessageType, tblTranslateMessageType WHERE tblMessage.fiMessageType = idMessageType AND tblTranslateMessageType.fiMessageType = idMessageType AND tblTranslateMessageType.fiLanguage = ? AND idMessage = ?;', [languageId, messageId]);
      console.log(typeName[0].idMessageType);
      // tslint:disable-next-line:max-line-length
      const [emailMessage] = await pool.query('SELECT * FROM tblMessageSubscription, tblEmailMessage WHERE idSubscription = fiSubscription AND fiMessage = ?;', [messageId, languageId]);
      const [fileMessage] = await pool.query('SELECT * FROM tblMessageSubscription, tblFileMessage WHERE idSubscription = fiSubscription AND fiMessage = ?;', [messageId, languageId]);

      // tslint:disable-next-line:max-line-length
      const [emailMessageType] = await pool.query('SELECT * FROM tblMessageTypeSubscription, tblEmailMessageType WHERE idSubscription = fiSubscription AND fiMessageType = ?;', [typeName[0].idMessageType, languageId]);
      const [fileMessageType] = await pool.query('SELECT * FROM tblMessageTypeSubscription, tblFileMessageType WHERE idSubscription = fiSubscription AND fiMessageType = ?;', [typeName[0].idMessageType, languageId]);

      emailMessage.forEach((element: { dtEmail: string; }) => {
        this.sendContactMail(element.dtEmail, message, this.getDateTime(), typeName[0].dtShownTypeName);
      });

      emailMessageType.forEach((element: { dtEmail: string; }) => {
        this.sendContactMail(element.dtEmail, message, this.getDateTime(), typeName[0].dtShownTypeName);
      });

      fileMessage.forEach((element: { dtUsername: string, dtPath: string; }) => {
        fs.writeFile(element.dtPath + '.txt', typeName[0].dtShownTypeName + '\n' + message + '\n\n' + new Date(), (err) => {
          if (err) {
            throw err;
          }
          console.log('The file has been saved!');
        });
      });

      fileMessageType.forEach((element: { dtUsername: string, dtPath: string; }) => {
        fs.writeFile(element.dtPath + '.txt', typeName[0].dtShownTypeName + '\n' + message + '\n\n' + new Date(), (err) => {
          if (err) {
            throw err;
          }
          console.log('The file has been saved!');
        });
      });
    }
    return rows[0];
  }

  private getDateTime(): string {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    return date + ' ' + time;
  }

  private async sendContactMail(emailAddress: string, message: string, dateTime: string, typeName: string) {
    console.log(emailAddress);
    console.log(dateTime);
    console.log(typeName);
    sgMail.setApiKey('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');
    const msg = {
      to: emailAddress,
      from: { email: 'noreply@messagehandler.lu', name: 'Message Handler' },
      subject: 'Subscription on MessageHandler',
      template_id: 'd-ec4c7b0ad2c047fb950ba2ab528826b1',
      dynamic_template_data: {
        message,
        dateTime,
        typeName
      }
    };
    // @ts-ignore
    return await sgMail.send(msg);
  }

  async translate(requestBody: any) {
    const { messageId, languageId, title, message } = requestBody;
    console.log(requestBody);

    await pool.query('CALL sp_createTranslation(?,?,?,?, @out_message);', [messageId, languageId, title, message]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    return rows[0];
  }

  async updateMessage(token: any, requestBody: any) {
    token = token.slice(7);
    token = JWT(token);
    console.log('here');
    console.log(requestBody);
    const { messageId, messageTypeId, message, parameter, shownTitle, languageId } = requestBody;

    await pool.query('CALL sp_updateMessage(?,?,?,?,?,?, @out_message);',
      [message, parameter, shownTitle, messageId, messageTypeId, languageId]);

    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    console.log(rows);
    return rows[0];
  }

  async deleteMessage(token: any, messageId: number) {
    token = token.slice(7);
    token = JWT(token);

    await pool.query('CALL sp_deleteMessage(?, @out_message);', [messageId]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    return rows[0];
  }
}
