import { Body, Controller, Get, Param, Post, Put, Headers, Delete, Header, UseGuards } from '@nestjs/common';
import { MessagesService } from './messages.service';
import { MessagesDto, OneMessageDto } from './messages.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('messages')
export class MessagesController {
  constructor(private messagesService: MessagesService) {
  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getAllMessages(
    @Headers() headers,
    ): Promise<[MessagesDto]> {
    return await this.messagesService.getAllMessages(headers.authorization);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('messages-only')
  async getAllMessagesOnly(
    @Headers() headers,
  ): Promise<[MessagesDto]> {
    return await this.messagesService.getAllMessagesOnly();
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('languages/:languageId')
  async getAllMessagesOfLanguage(
    @Headers() headers,
    @Param() params,
  ): Promise<[MessagesDto]> {
    return await this.messagesService.getAllMessagesOfLanguage(headers.authorization, params.languageId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/:messageId')
  async getOneMessage(
    @Headers() headers,
    @Param() params,
  ): Promise<OneMessageDto> {
    return await this.messagesService.getOneMessage(headers.authorization, params.messageId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/:messageId/:languageId')
  async getOneMessageOfLanguage(
    @Headers() headers,
    @Param() params,
  ): Promise<OneMessageDto> {
    return await this.messagesService.getOneMessageOfLanguage(headers.authorization, params.messageId, params.languageId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createMessage(
    @Headers() headers,
    @Body() requestBody,
  ): Promise<any> {
    return await this.messagesService.createMessage(headers.authorization, requestBody);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('send')
  async sendMessage(
    @Headers() headers,
    @Body() requestBody,
    ): Promise<any> {
    return await this.messagesService.sendMessage(headers.authorization, requestBody);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('translate')
  async translate(
    @Headers() headers,
    @Body() requestBody,
    ): Promise<any> {
    return await this.messagesService.translate(requestBody);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('update')
  async updateMessage(
    @Headers() headers,
    @Body() requestBody,
    ): Promise<any> {
    return await this.messagesService.updateMessage(headers.authorization, requestBody);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/:messageId')
  async deleteMessage(
    @Headers() headers,
    @Param() params,
  ): Promise<any> {
    return await this.messagesService.deleteMessage(headers.authorization, params.messageId);
  }
}
