export interface MessagesDto {
  idMessage: number;
  dtParameter: string;
  dtParameterCount: number;
  fiMessageType: number;
  fiLanguage: string;
  dtTitle: string;
  dtText: string;
}

export interface OneMessageDto {
  idMessage: number;
  dtParameter: string;
  dtParameterCount: number;
  fiMessageType: number;
  fiLanguage: string;
  dtTitle: string;
  dtText: string;
}
