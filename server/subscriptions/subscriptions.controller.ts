import { Body, Controller, Delete, Get, Headers, Param, Post, Put, UseGuards } from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { SubscriptionsDto } from './subscriptions.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('subscriptions')
export class SubscriptionsController {
  constructor(private subscriptionsService: SubscriptionsService) {
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('message')
  async getMessageSubscriptions(
    @Headers() headers,
  ): Promise<[SubscriptionsDto]> {
    return this.subscriptionsService.getMessageSubscriptions(headers.authorization);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('message-type')
  async getMessageTypeSubscriptions(
    @Headers() headers,
  ): Promise<[SubscriptionsDto]> {
    return this.subscriptionsService.getMessageTypeSubscriptions(headers.authorization);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('message')
  async createMessageSubscription(
    @Headers() headers,
    @Body() requestBody
  ): Promise<any> {
    return await this.subscriptionsService.createMessageSubscription(headers.authorization, requestBody);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('message-type')
  async createMessageTypeSubscription(
    @Headers() headers,
    @Body() requestBody
  ): Promise<any> {
    return await this.subscriptionsService.createMessageTypeSubscription(headers.authorization, requestBody);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('message/:subscriptionId')
  async deleteMessageSubscription(
    @Headers() headers,
    @Param() params
  ): Promise<any> {
    return await this.subscriptionsService.deleteMessageSubscription(params.subscriptionId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('message-type/:subscriptionId')
  async deleteMessageTypeSubscription(
    @Headers() headers,
    @Param() params
  ): Promise<any> {
    return await this.subscriptionsService.deleteMessageTypeSubscription(params.subscriptionId);
  }
}
