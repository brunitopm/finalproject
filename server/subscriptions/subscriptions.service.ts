import { Injectable } from '@nestjs/common';
import { SubscriptionsDto } from './subscriptions.dto';
import { pool } from '../DBPool';
import * as JWT from 'jwt-decode';

@Injectable()
export class SubscriptionsService {
  async getMessageSubscriptions(token: any): Promise<[SubscriptionsDto]> {
    token = token.slice(7);
    token = JWT(token);

    // tslint:disable-next-line:max-line-length
    const rows = await pool.query('SELECT idSubscription, dtTitle, dtEmail FROM tblMessage, tblMessageSubscription, tblEmailMessage WHERE idSubscription = fiSubscription AND idMessage = fiMessage AND tblMessageSubscription.fiUser = ?;', [token.userId]);
    return rows[0];
  }

  async getMessageTypeSubscriptions(token: any): Promise<[SubscriptionsDto]> {
    token = token.slice(7);
    token = JWT(token);

    // tslint:disable-next-line:max-line-length
    const rows = await pool.query('SELECT idSubscription, dtTypeName, dtEmail FROM tblMessageType, tblMessageTypeSubscription, tblEmailMessageType WHERE idSubscription = fiSubscription AND idMessageType = fiMessageType AND tblMessageTypeSubscription.fiUser = ?;', [token.userId]);
    return rows[0];
  }

  async createMessageSubscription(token: any, requestBody: any): Promise<any> {
    token = token.slice(7);
    token = JWT(token);

    const { messageId, email } = requestBody;

    await pool.query('CALL sp_createSubscriptionMessage(?,?,?, @out_message);', [messageId, token.userId, email]);
    const result = await pool.query('SELECT @out_message;', []);

    return result[0];
  }

  async createMessageTypeSubscription(token: any, requestBody: any): Promise<any> {
    token = token.slice(7);
    token = JWT(token);
    console.log('holla');
    const { messageTypeId, email } = requestBody;

    await pool.query('CALL sp_createSubscriptionMessageType(?,?,?, @out_message);', [messageTypeId, token.userId, email]);
    const result = await pool.query('SELECT @out_message;', []);

    return result[0];
  }

  async deleteMessageSubscription(subscriptionId: number): Promise<any> {
    await pool.query('CALL sp_deleteSubscriptionMessage(?, @out_message);', [subscriptionId]);
    const result = await pool.query('SELECT @out_message;', []);

    return result[0];
  }

  async deleteMessageTypeSubscription(subscriptionId: number): Promise<any> {
    await pool.query('CALL sp_deleteSubscriptionMessageType(?, @out_message);', [subscriptionId]);
    const result = await pool.query('SELECT @out_message;', []);

    return result[0];
  }
}
