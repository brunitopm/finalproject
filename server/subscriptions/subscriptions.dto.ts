export interface SubscriptionsDto {
  subscriptionId: number;
  messageId: number;
  userId: number;
  email: string;
}
