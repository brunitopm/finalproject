const DBOptions = {
  host: process.env.DBHOST || 'localhost',
  user: process.env.DBUSER || 'root',
  pw: process.env.DBPASSWORD || 'mysqlrootpw',
  database: process.env.DBNAME || 'dbMessageHandler'
};

module.exports = DBOptions;
