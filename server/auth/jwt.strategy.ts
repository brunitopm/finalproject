import {ExtractJwt, Strategy, VerifiedCallback} from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
// import { jwtSecret } from "../helpFunctions";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'key',
    });
  }

  async validate(payload: any) {
    return { username: payload.username, userId: payload.userId, languageId: payload.languageId };
  }
}
