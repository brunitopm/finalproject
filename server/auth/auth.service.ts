import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {

  constructor(private readonly usersService: UsersService, private readonly jwtService: JwtService) {
  }

  async validateUser(username: string, password: string) {
    return await this.usersService.findByLogin(username, password);
  }

  async login(user: any) {
    const payload = { username: user.username, userId: user.userId, languageId: user.languageId };
    return {
      access_token: this.jwtService.sign(payload)
    };
  }

}
