import { Body, Controller, Delete, Get, Headers, Param, Post, Put, UseGuards } from '@nestjs/common';
import { LanguagesDto } from './languages.dto';
import { LanguagesService } from './languages.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('languages')
export class LanguagesController {
  constructor(private languagesService: LanguagesService) {
  }

  @Get()
  async getAllLanguages(
    @Headers() headers,
  ): Promise<[LanguagesDto]> {
    return await this.languagesService.getAllLanguages();
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('used/:messageId')
  async getLanguagesUsed(
    @Headers() headers,
    @Param() params,
  ): Promise<[LanguagesDto]> {
    return await this.languagesService.getLanguagesUsed(params.messageId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('not-used/:messageId')
  async getLanguagesNotUsed(
    @Headers() headers,
    @Param() params,
  ): Promise<[LanguagesDto]> {
    return await this.languagesService.getLanguagesNotUsed(params.messageId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createLanguage(
    @Headers() headers,
    @Body() requestBody
  ): Promise<any> {
    return await this.languagesService.createLanguage(requestBody);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put()
  async updateLanguage(
    @Headers() headers,
    @Body() requestBody
  ): Promise<any> {
    return await this.languagesService.updateLanguage(requestBody);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteLanguage(
    @Headers() headers,
    @Param() params
  ): Promise<any> {
    return await this.languagesService.deleteLanguage(params.id);
  }
}
