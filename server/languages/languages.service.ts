import { Injectable } from '@nestjs/common';
import { pool } from '../DBPool';
import { LanguagesDto } from './languages.dto';

@Injectable()
export class LanguagesService {
  async getAllLanguages(): Promise<[LanguagesDto]> {
    const [rows, fields] = await pool.query('SELECT * FROM tblLanguage;', []);
    return rows;
  }

  async getLanguagesUsed(messageId: number): Promise<[LanguagesDto]> {
    // tslint:disable-next-line:max-line-length
    const [rows, fields] = await pool.query('SELECT idLanguage, dtLanguageName FROM tblLanguage WHERE EXISTS (SELECT 1 FROM tblTranslate WHERE idLanguage = fiLanguage AND fiMessage = ?);', [messageId]);
    return rows;
  }

  async getLanguagesNotUsed(messageId: number): Promise<[LanguagesDto]> {
    // tslint:disable-next-line:max-line-length
    const [rows, fields] = await pool.query('SELECT idLanguage, dtLanguageName FROM tblLanguage WHERE NOT EXISTS (SELECT 1 FROM tblTranslate WHERE idLanguage = fiLanguage AND fiMessage = ?)', [messageId]);
    return rows;
  }

  async createLanguage(requestBody: any) {
    const { languageId, languageName } = requestBody;

    await pool.query('CALL sp_createLanguage(?,?, @out_message);', [languageId, languageName]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    console.log(rows);

    return rows[0];
  }

  async updateLanguage(requestBody: any) {
    const { languageId, languageName } = requestBody;

    await pool.query('CALL sp_updateLanguage(?,?, @out_message);', [languageId, languageName]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    return rows[0];
  }

  async deleteLanguage(languageId: number) {
    await pool.query('CALL sp_deleteLanguage(?, @out_message);', [languageId]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    return rows[0];
  }
}
