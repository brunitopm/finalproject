export interface LanguagesDto {
  languageId: string;
  languageName: string;
}
