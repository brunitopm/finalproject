import { Injectable } from '@nestjs/common';
import { pool } from '../DBPool';
import { MessageTypesDto } from './message-types.dto';
import * as JWT from 'jwt-decode';

@Injectable()
export class MessageTypesService {
  async getAllMessageTypes(token: any): Promise<[MessageTypesDto]> {
    token = token.slice(7);
    token = JWT(token);
    const [rows, fields] = await pool.query('SELECT * FROM tblMessageType, tblTranslateMessageType WHERE idMessageType = fiMessageType AND fiLanguage = ? ORDER BY idMessageType;', [token.languageId]);

    return rows;
  }

  async getOneMessageTypes(token: any, messageTypeId: number): Promise<[MessageTypesDto]> {
    token = token.slice(7);
    token = JWT(token);
    const [rows, fields] = await pool.query('SELECT * FROM tblMessageType, tblTranslateMessageType WHERE idMessageType = fiMessageType AND fiMessageType = ? AND fiLanguage = ? ORDER BY idMessageType;', [messageTypeId, token.languageId]);

    return rows;
  }

  async createMessageType(requestBody: any): Promise<any> {
    const { messageTypeName, shownTypeName, languageId } = requestBody;
    console.log('here');
    await pool.query('CALL sp_createMessageType(?, ?, ?, @out_message);', [messageTypeName, shownTypeName, languageId]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    return rows;
  }

  async updateMessageType(requestBody: any): Promise<any> {
    const { messageTypeId, messageTypeName } = requestBody;

    await pool.query('CALL sp_updateMessageType(?,?, @out_message);', [messageTypeId, messageTypeName]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    return rows;
  }

  async deleteMessageType(messageTypeId: number): Promise<any> {
    await pool.query('CALL sp_deleteMessageType(?, @out_message);', [messageTypeId]);
    const [rows, fields] = await pool.query('SELECT @out_message;', []);

    return rows;
  }
}
