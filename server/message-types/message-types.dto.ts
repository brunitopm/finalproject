export interface MessageTypesDto {
  messageTypeId: number;
  typeName: string;
}
