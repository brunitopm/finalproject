import { Controller, Delete, Get, Post, Headers, Body, Param, Put, UseGuards } from '@nestjs/common';
import { MessageTypesService } from './message-types.service';
import { MessageTypesDto } from './message-types.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('message-types')
export class MessageTypesController {
  constructor(private messageTypesService: MessageTypesService) {
  }

  @Get()
  async getAllMessageTypes(@Headers() headers): Promise<[MessageTypesDto]> {
    return await this.messageTypesService.getAllMessageTypes(headers.authorization);
  }

  @Get(':messageTypeId')
  async getOneMessageType(@Headers() headers, @Param() params): Promise<[MessageTypesDto]> {
    return await this.messageTypesService.getOneMessageTypes(headers.authorization, params.messageTypeId);
  }

  @Post()
  async createMessageType(@Body() requestBody): Promise<any> {
    return await this.messageTypesService.createMessageType(requestBody);
  }

  @Put()
  async updateMessageType(@Body() requestBody): Promise<any> {
    return await this.messageTypesService.updateMessageType(requestBody);
  }

  @Delete(':messageTypeId')
  async deleteMessageType(@Param() params): Promise<any> {
    return await this.messageTypesService.deleteMessageType(params.messageTypeId);
  }
}
