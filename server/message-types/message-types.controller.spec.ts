import { Test, TestingModule } from '@nestjs/testing';
import { MessageTypesController } from './message-types.controller';

describe('MessageTypes Controller', () => {
  let controller: MessageTypesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MessageTypesController],
    }).compile();

    controller = module.get<MessageTypesController>(MessageTypesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
