import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { MessagesService } from '../messages.service';
import { SubscriptionsService } from '../subscriptions.service';
import { MessageTypes } from '../message-types/message-types.component';
import { Messages } from '../messages/messages.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageTypesService } from '../message-types.service';

export interface SubMessages {
  idSubscription: string;
  dtTitle: string;
  dtEmail: string;
}

export interface SubMessageTypes {
  idSubscription: string;
  dtTypeName: string;
  dtEmail: string;
}

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.sass']
})
export class SubscriptionsComponent implements OnInit {

  public messages: Messages[];
  public messageTypes: MessageTypes[];
  public subMessages: SubMessages[];
  public subMessageTypes: SubMessageTypes[];

  public displayedColumns: string[] = ['title', 'email', 'button'];
  public displayedColumns2: string[] = ['messageType', 'email', 'button'];

  constructor(
    private cookie: CookieService,
    private router: Router,
    private readonly messagesService: MessagesService,
    private readonly messageTypesService: MessageTypesService,
    private readonly subscriptionsService: SubscriptionsService) {
  }

  public subscriptionMessageForm: FormGroup = new FormGroup({
    messageId: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
  });

  public subscriptionMessageTypeForm: FormGroup = new FormGroup({
    messageTypeId: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
  });

  ngOnInit(): void {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.getSubscriptionMessages();
        this.getSubscriptionMessageTypes();
        this.getMessages();
        this.getMessageTypes();
      } else {
        this.router.navigate(['']).then();
      }
    }
  }

  private getMessages() {
    this.messagesService.getAllMessagesOnly().subscribe(
      data => this.setMessages(data),
      error => console.log(error),
    );
  }

  private getMessageTypes() {
    this.messageTypesService.getAllMessageTypes().subscribe(
      data => this.setMessageTypes(data),
      error => console.log(error),
    );
  }

  private getSubscriptionMessages() {
    this.subscriptionsService.getSubscriptionMessages().subscribe(
      data => this.setSubMessages(data),
      error => console.log(error),
    );
  }

  private getSubscriptionMessageTypes() {
    this.subscriptionsService.getSubscriptionMessageTypes().subscribe(
      data => this.setSubMessageTypes(data),
      error => console.log(error),
    );
  }

  public createSubscriptionMessage() {
    if (this.subscriptionMessageForm.valid) {
      this.subscriptionsService.createSubscriptionMessage(this.subscriptionMessageForm.value).subscribe(
        data => this.getSubscriptionMessages(),
        error => console.log(error),
      );
    } else {
      console.log('form not valid');
    }
  }

  public createSubscriptionMessageType() {
    if (this.subscriptionMessageTypeForm.valid) {
      this.subscriptionsService.createSubscriptionMessageType(this.subscriptionMessageTypeForm.value).subscribe(
        data => this.getSubscriptionMessageTypes(),
        error => console.log(error),
      );
    } else {
      console.log('form not valid');
    }
  }

  public deleteSubscriptionMessage(subscriptionId: number) {
    this.subscriptionsService.deleteSubscriptionMessage(subscriptionId).subscribe(
      data => this.getSubscriptionMessages(),
      error => console.log(error),
    );
  }

  public deleteSubscriptionMessageType(subscriptionId: number) {
    this.subscriptionsService.deleteSubscriptionMessageType(subscriptionId).subscribe(
      data => this.getSubscriptionMessageTypes(),
      error => console.log(error),
    );
  }

  private setSubMessages(data: any) {
    if (data === null) {
      console.log('empty');
    } else {
      this.subMessages = data;
    }
  }

  private setSubMessageTypes(data: any) {
    if (data === null) {
      console.log('empty');
    } else {
      this.subMessageTypes = data;
    }
  }

  private setMessages(data: any) {
    console.log(data);
    if (data === null) {
      console.log('empty');
    } else {
      this.messages = data;
    }
  }

  private setMessageTypes(data: any) {
    if (data === null) {
      console.log('empty');
    } else {
      this.messageTypes = data;
    }
  }
}
