import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  url: string;

  constructor(private http: HttpClient, private cookie: CookieService) {
  }

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.cookie.get('token')
    })
  };

  public getAllMessages() {
    this.url = '/api/messages';
    return this.http.get(this.url, this.httpOptions);
  }

  public getAllMessagesOnly() {
    this.url = '/api/messages/messages-only';
    return this.http.get(this.url, this.httpOptions);
  }

  public getAllLanguageMessages(languageId: string) {
    this.url = '/api/messages/languages/' + languageId;
    return this.http.get(this.url, this.httpOptions);
  }

  public getOneMessage(messageId: any) {
    this.url = '/api/messages/' + messageId;
    return this.http.get(this.url, this.httpOptions);
  }

  public getOneLanguageMessage(messageId: any, languageId: any) {
    this.url = '/api/messages/' + messageId + '/' + languageId;
    return this.http.get(this.url, this.httpOptions);
  }

  public createMessage(data: any) {
    this.url = '/api/messages';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public sendMessage(data: any) {
    this.url = '/api/messages/send';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public translate(data: any) {
    this.url = '/api/messages/translate';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public updateMessage(data: any) {
    this.url = '/api/messages/update';
    return this.http.put(this.url, data, this.httpOptions);
  }

  public deleteMessage(messageId: number) {
    this.url = '/api/messages/' + messageId;
    return this.http.delete(this.url, this.httpOptions);
  }

}
