import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../messages.service';
import { MessageTypes } from '../message-types/message-types.component';
import { MessageTypesService } from '../message-types.service';
import { Languages } from '../languages/languages.component';
import { LanguagesService } from '../languages.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

export interface Messages {
  idMessage: number;
  dtText: string;
  dtTitle: string;
  dtShownTitle: string;
  dtTypeName: string;
  dtShownTypeName: string;
  dtParameter: string;
  dtParameterCount: number;
}

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit {

  public messages: Messages[] = [];
  public languages: Languages[] = [];
  public messageTypes: MessageTypes[] = [];

  public error: string;

  public displayedColumns: string[] = ['shownTitle', 'messageType', 'button'];

  constructor(
    private messagesService: MessagesService,
    private messageTypesService: MessageTypesService,
    private languagesService: LanguagesService,
    private cookie: CookieService,
    private router: Router,
  ) {
  }

  public createMessageForm: FormGroup = new FormGroup({
    typeId: new FormControl('', [Validators.required]),
    languageId: new FormControl('', [Validators.required]),
    title: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    shownTitle: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    text: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    parameter: new FormControl('', [Validators.required, Validators.maxLength(5)]),
  });

  ngOnInit() {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.messagesService.getAllMessages().subscribe(
          data => this.setMessages(data),
          error => this.error = error,
        );

        this.messageTypesService.getAllMessageTypes().subscribe(
          data => this.setMessageTypes(data),
          error => console.log(error),
        );

        this.languagesService.getAllLanguages().subscribe(
          data => this.setLanguages(data),
          error => console.log(error),
        );
      } else {
        this.router.navigate(['']).then();
      }
    }
  }

  public createMessage() {
    if (this.createMessageForm.valid) {
      this.messagesService.createMessage(this.createMessageForm.value).subscribe(
        data => this.setError(data),
        error => console.log(error),
      );
    }
  }

  public deleteMessage(id: number) {
    this.messagesService.deleteMessage(id).subscribe(
      data => this.setMessages(data),
      error => console.log(error),
    );
  }

  private setMessages(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.messages = data;
    }
  }

  private setError(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.error = data['@out_message'];
    }
  }

  private setMessageTypes(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.messageTypes = data;
    }
  }

  private setLanguages(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.languages = data;
    }
  }
}
