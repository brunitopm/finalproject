import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class MessageTypesService {

  constructor(private cookie: CookieService, private http: HttpClient) {
  }
  url: string;

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.getCookie(),
    })
  };

  private getCookie() {
    return this.cookie.get('token');
  }

  public getAllMessageTypes() {
    this.url = '/api/message-types';
    return this.http.get(this.url, this.httpOptions);
  }

  public createMessageType(data: any) {
    this.url = '/api/message-types';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public deleteMessageType(messageTypeId: number) {
    this.url = '/api/message-types/' + messageTypeId;
    return this.http.delete(this.url, this.httpOptions);
  }

}
