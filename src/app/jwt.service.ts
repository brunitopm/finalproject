import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor(private cookieService: CookieService) {
  }

  public isAuthenticated(): boolean {
    return !(this.cookieService.get('token') === null || this.cookieService.get('token') === undefined);

  }

  public getAuthToken(): string {
    return this.cookieService.get('token');
  }
}
