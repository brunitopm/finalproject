import { Component, OnInit } from '@angular/core';
import { MessageTypesService } from '../message-types.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Languages } from '../languages/languages.component';
import { LanguagesService } from '../languages.service';

export interface MessageTypes {
  idMessageType: number;
  dtTypeName: string;
  dtShownTypeName: string;
}

@Component({
  selector: 'app-message-types',
  templateUrl: './message-types.component.html',
  styleUrls: ['./message-types.component.sass']
})
export class MessageTypesComponent implements OnInit {

  public messageTypes: MessageTypes[] = [];
  public languages: Languages[] = [];
  public error = '';

  public displayedColumns = ['messageTypeId', 'messageTypeName', 'button'];

  constructor(
    private cookie: CookieService,
    private router: Router,
    private messageTypesService: MessageTypesService,
    private languagesService: LanguagesService
  ) {
  }

  public createMessageTypeForm: FormGroup = new FormGroup({
    messageTypeName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
    shownTypeName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
    languageId: new FormControl('', [Validators.required, Validators.maxLength(2)]),
  });

  ngOnInit(): void {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.messageTypesService.getAllMessageTypes().subscribe(
          data => this.setData(data),
          error => console.log(error),
        );

        this.languagesService.getAllLanguages().subscribe(
          data => this.setLanguages(data),
          error => console.log(error),
        );
      } else {
        this.router.navigate(['']).then();
      }
    }
  }

  createMessageType() {
    console.log(this.createMessageTypeForm.valid);
    if (this.createMessageTypeForm.valid) {
      this.messageTypesService.createMessageType(this.createMessageTypeForm.value).subscribe(
        data => this.setError(data),
        error => console.log(error),
      );
    }
  }

  deleteMessageType(messageTypeId: number) {
    console.log(messageTypeId);
    this.messageTypesService.deleteMessageType(messageTypeId).subscribe(
      data => this.setError(data),
      error => console.log(error),
    );
  }

  private setData(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.messageTypes = data;
    }
  }

  private setLanguages(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.languages = data;
    }
  }

  private setError(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.error = data[0]['@out_message'];
    }
  }
}
