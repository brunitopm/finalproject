import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.sass']
})
export class TopBarComponent implements OnInit {

  public navlinks = [
    { label: 'Messages', path: 'messages' },
    { label: 'Translate', path: 'translate' },
    { label: 'Send Message', path: 'send-message' },
    { label: 'Message Types', path: 'message-types' },
    { label: 'Subscriptions', path: 'subscriptions' },
    { label: 'Languages', path: 'languages' },
  ];

  constructor(public cookie: CookieService, private router: Router) {
  }

  ngOnInit(): void {
  }

  deleteCookie() {
    this.cookie.delete('token');
    setTimeout(() => {
      this.router.navigate(['']).then();
    }, 500);
  }

}
