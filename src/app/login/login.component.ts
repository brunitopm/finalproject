import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../users.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

export interface Login {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  public error: string;

  constructor(private cookie: CookieService, private router: Router, private readonly usersService: UsersService) {
  }

  public loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    password: new FormControl('', [Validators.required, Validators.maxLength(50)]),
  });

  ngOnInit(): void {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.router.navigate(['/messages']).then();
      }
    }
  }

  async login() {
    if (this.loginForm.valid) {
      this.usersService.login(this.loginForm.value).subscribe(
        data => this.setError(data),
        error => console.log(error),
      );
    }
  }

  public setError(data: any) {
    if (data === null) {
      console.log('hey');
    } else {
      if (data.access_token) {
        console.log(data.access_token);
        this.cookie.set('token', data.access_token);
        this.router.navigate(['/messages']).then();
      }
    }
  }
}
