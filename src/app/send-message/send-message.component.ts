import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Messages } from '../messages/messages.component';
import { MessagesService } from '../messages.service';
import { Languages } from '../languages/languages.component';
import { Message } from '../message-detail/message-detail.component';
import { LanguagesService } from '../languages.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.sass']
})
export class SendMessageComponent implements OnInit {

  public messages: Messages[];
  public languages: Languages[];
  public message: Message;

  public languageId: string;

  public error;

  constructor(private cookie: CookieService, private router: Router, private readonly messagesService: MessagesService, private readonly languagesService: LanguagesService) {
  }

  public sendMessageForm: FormGroup = new FormGroup({
    languageId: new FormControl('', [Validators.required]),
    messageId: new FormControl('', [Validators.required]),
    parameters: new FormControl('', [Validators.required, Validators.maxLength(100)]),
  });

  ngOnInit(): void {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.languagesService.getAllLanguages().subscribe(
          data => this.setLanguages(data),
          error => console.log(error),
        );
      } else {
        this.router.navigate(['']).then();
      }
    }
  }

  public getAllLanguageMessages(languageId: string): void {
    this.languageId = languageId;
    this.messagesService.getAllLanguageMessages(languageId).subscribe(
      data => this.setMessages(data),
      error => console.log(error),
    );
  }

  public getOneLanguageMessage(messageId: number): void {
    this.messagesService.getOneLanguageMessage(messageId, this.languageId).subscribe(
      data => this.setMessage(data),
      error => console.log(error),
    );
  }

  public sendMessage() {
    if (this.sendMessageForm.valid) {
      this.messagesService.sendMessage(this.sendMessageForm.value).subscribe(
        data => this.setError(data),
        error => console.log(error)
      );
    } else {
      console.log('the form is not valid');
    }
  }

  private setMessages(data: any): void {
    if (data === null || data.length === 0) {
      this.messages = [];
    } else {
      this.messages = data;
    }
  }

  private setMessage(data: any): void {
    console.log(data);
    if (data === null || data.length === 0) {
      console.log('hey');
    } else {
      this.message = data;
    }
  }

  private setLanguages(data: any): void {
    if (data === null || data.length === 0) {
      console.log('No languages received.');
    } else {
      this.languages = data;
    }
  }

  setError(data) {
    if (data === null) {
      console.log('data is null');
    } else {
      this.error = data['@out_message'];
    }
  }
}
