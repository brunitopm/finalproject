import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { LanguagesService } from '../languages.service';
import { Languages } from '../languages/languages.component';
import { UsersService } from '../users.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  public languages: Languages[] = [];
  public error: string;

  constructor(private cookie: CookieService, private readonly languagesService: LanguagesService, private readonly usersService: UsersService, private router: Router) {
  }

  public registerForm: FormGroup = new FormGroup({
    firstname: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    lastname: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    username: new FormControl('', [Validators.required, Validators.maxLength(50)]),
    password: new FormControl('', [Validators.required, Validators.maxLength(50)]),
    email: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
    address: new FormControl(''),
    phoneNumber: new FormControl('', [Validators.required, Validators.maxLength(15)]),
    languageId: new FormControl('', [Validators.required]),
  });

  getFormValidationErrors() {
    Object.keys(this.registerForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.registerForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }

  ngOnInit(): void {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.router.navigate(['/messages']).then();
      } else {
        this.languagesService.getAllLanguages().subscribe(
          data => this.setLanguages(data),
          error => console.log(error),
        );
      }
    }
  }

  async register() {
    this.getFormValidationErrors();
    if (this.registerForm.valid) {
      console.log(this.registerForm.value);
      this.usersService.register(this.registerForm.value).subscribe(
        data => this.setError(data),
        error => console.log(error),
      );
    }
  }

  private setLanguages(data: any): void {
    if (data === null) {
      console.log('hey');
    } else {
      this.languages = data;
    }
  }

  private setError(data: any): void {
    if (data === null) {
      console.log('hey');
    } else {
      this.error = data[0]['@out_message'];
    }
  }
}
