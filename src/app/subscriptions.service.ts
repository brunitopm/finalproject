import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsService {

  private url: string;

  constructor(private http: HttpClient) {
  }

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    })
  };

  public getSubscriptionMessages() {
    this.url = '/api/subscriptions/message';
    return this.http.get(this.url, this.httpOptions);
  }

  public getSubscriptionMessageTypes() {
    this.url = '/api/subscriptions/message-type';
    return this.http.get(this.url, this.httpOptions);
  }

  public createSubscriptionMessage(data: any) {
    this.url = '/api/subscriptions/message';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public createSubscriptionMessageType(data: any) {
    this.url = '/api/subscriptions/message-type';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public deleteSubscriptionMessage(subscriptionId: number) {
    this.url = '/api/subscriptions/message/' + subscriptionId;
    return this.http.delete(this.url, this.httpOptions);
  }

  public deleteSubscriptionMessageType(subscriptionId: number) {
    this.url = '/api/subscriptions/message-type/' + subscriptionId;
    return this.http.delete(this.url, this.httpOptions);
  }
}
