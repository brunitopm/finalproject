import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private url: string;

  constructor(private http: HttpClient) {
  }

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    })
  };

  public login(data: any) {
    this.url = '/api/auth/login';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public register(data: any) {
    this.url = '/api/users/register';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public updateUser(data: any) {
    this.url = '/api/languages';
    return this.http.put(this.url, data, this.httpOptions);
  }

  public deleteUser(userId: string) {
    this.url = '/api/languages/' + userId;
    return this.http.delete(this.url, this.httpOptions);
  }
}
