import { TestBed } from '@angular/core/testing';

import { MessageTypesService } from './message-types.service';

describe('MessageTypesService', () => {
  let service: MessageTypesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessageTypesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
