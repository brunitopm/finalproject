import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { MessagesService } from '../messages.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Languages } from '../languages/languages.component';
import { MessageTypes } from '../message-types/message-types.component';
import { LanguagesService } from '../languages.service';
import { MessageTypesService } from '../message-types.service';

export interface Message {
  idMessage: number;
  dtText: string;
  dtTitle: string;
  dtTypeName: string;
  dtParameter: string;
  dtParameterCount: number;
  fiLanguage: string;
  fiMessageType: number;
  dtLanguageName: string;
}

@Component({
  selector: 'app-message-detail',
  templateUrl: './message-detail.component.html',
  styleUrls: ['./message-detail.component.sass']
})
export class MessageDetailComponent implements OnInit {

  public messageId: number;
  public chosenLanguage: string;
  public message: Message;
  public languages: Languages[];
  public messageTypes: MessageTypes[];
  public error;

  public updateMessageForm: FormGroup = new FormGroup({
    shownTitle: new FormControl('', [Validators.required]),
    message: new FormControl('', [Validators.required]),
    messageId: new FormControl('', [Validators.required]),
    parameter: new FormControl('', [Validators.required]),
    messageTypeId: new FormControl('', [Validators.required]),
    languageId: new FormControl('', [Validators.required]),
  });

  constructor(
    private route: ActivatedRoute,
    private cookie: CookieService,
    private router: Router,
    private readonly messagesService: MessagesService,
    private readonly languagesService: LanguagesService,
    private readonly messageTypesService: MessageTypesService
  ) {
  }

  ngOnInit(): void {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.route.paramMap.subscribe(params => {
          // tslint:disable-next-line:radix
          this.messageId = parseInt(params.get('messageId'));
          this.updateMessageForm.controls.messageId.setValue(this.messageId);
        });
        // this.getOneMessage(this.messageId);
        this.getAllMessageTypes();
        this.getAllLanguages();
      } else {
        this.router.navigate(['']).then();
      }
    }
  }

  private getOneMessage(messageId: number) {
    this.messagesService.getOneMessage(messageId).subscribe(
      data => this.setMessage(data),
      error => this.error = error,
    );
  }

  private getAllMessageTypes() {
    this.messageTypesService.getAllMessageTypes().subscribe(
      data => this.setMessageTypes(data),
      error => console.log(error),
    );
  }

  private getAllLanguages() {
    this.languagesService.getAllLanguages().subscribe(
      data => this.setLanguages(data),
      error => console.log(error),
    );
  }

  public getOneLanguageMessage() {
    this.chosenLanguage = this.updateMessageForm.value.languageId;
    this.messagesService.getOneLanguageMessage(this.messageId, this.chosenLanguage).subscribe(
      data => this.setMessage(data),
      error => console.log(error),
    );
  }

  private setMessage(data) {
    console.log('Message');
    console.log(data);
    if (data === null) {
      this.error = 'This message does not exist';
    } else {
      this.message = data;

      this.updateMessageForm.controls.shownTitle.setValue(data.dtShownTitle);
      this.updateMessageForm.controls.message.setValue(data.dtText);
      this.updateMessageForm.controls.parameter.setValue(data.dtParameter);
      this.updateMessageForm.controls.messageTypeId.setValue(data.fiMessageType);

      this.error = '';
    }
  }

  private setMessageTypes(data: any): void {
    console.log('Message Types');
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.messageTypes = data;
    }
  }

  private setLanguages(data: any): void {
    console.log('Languages');
    console.log(data);
    // for (const la of data) {
    //   console.log(la);
    // }
    if (data === null) {
      console.log('hey');
    } else {
      this.languages = data;
    }
  }


  public updateMessage() {
    if (this.updateMessageForm.valid) {
      console.log(this.updateMessageForm.value);
      this.messagesService.updateMessage(this.updateMessageForm.value).subscribe(
        data => console.log(data),
        error => console.log(error),
      );
    } else {
      console.log('bruh');
    }
  }
}
