import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LanguagesService {

  private url: string;

  constructor(private http: HttpClient) {
  }

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    })
  };

  public getAllLanguages() {
    this.url = '/api/languages';
    return this.http.get(this.url, this.httpOptions);
  }

  public getLanguagesNotUsed(messageId: number) {
    this.url = '/api/languages/not-used/' + messageId;
    return this.http.get(this.url, this.httpOptions);
  }

  public createLanguage(data: any) {
    this.url = '/api/languages';
    return this.http.post(this.url, data, this.httpOptions);
  }

  public updateLanguage(id: string, name: string) {
    const language = {
      languageId: id,
      languageName: name,
    };
    this.url = '/api/languages';
    return this.http.put(this.url, language, this.httpOptions);
  }

  public deleteLanguage(languageId: string) {
    this.url = '/api/languages/' + languageId;
    return this.http.delete(this.url, this.httpOptions);
  }
}
