import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Languages } from '../languages/languages.component';
import { CookieService } from 'ngx-cookie-service';
import { MessagesService } from '../messages.service';
import { LanguagesService } from '../languages.service';
import { Router } from '@angular/router';
import { Messages } from '../messages/messages.component';
import { Message } from '../message-detail/message-detail.component';

@Component({
  selector: 'app-translate-message',
  templateUrl: './translate-message.component.html',
  styleUrls: ['./translate-message.component.sass']
})
export class TranslateMessageComponent implements OnInit {

  public message: Message;
  public messages: Messages[];
  public languages: Languages[];
  public error: string;

  constructor(
    private cookie: CookieService,
    private router: Router,
    private readonly messagesService: MessagesService,
    private readonly languagesService: LanguagesService,
  ) {
  }

  public translateForm: FormGroup = new FormGroup({
    languageId: new FormControl('', [Validators.required]),
    title: new FormControl('', [Validators.required]),
    message: new FormControl('', [Validators.required]),
    messageId: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.getAllMessagesOnly();
      } else {
        this.router.navigate(['']).then();
      }
    }
  }

  private getAllMessagesOnly() {
    this.messagesService.getAllMessagesOnly().subscribe(
      data => this.setMessages(data),
      error => this.setError(error),
    );
  }

  public createTranslation() {
    if (this.translateForm.valid) {
      this.messagesService.translate(this.translateForm.value).subscribe(
        data => console.log(data),
        error => console.log(error),
      );
    } else {
      console.log('translate form is not valid');
    }
  }

  public getLanguagesNotUsed(messageId: number) {
    this.translateForm.controls.messageId.setValue(messageId);
    this.languagesService.getLanguagesNotUsed(messageId).subscribe(
      data => this.setLanguages(data),
      error => console.log(error),
    );
  }

  private setMessages(data: any) {
    console.log(data);
    if (data === null) {
      console.log('no message found');
    } else {
      this.messages = data;
      console.log(this.messages);
    }
  }

  private setMessage(data: any) {
    if (data === null) {
      console.log('no message found');
    } else {
      this.message = data;
    }
  }

  private setLanguages(data: any) {
    if (data === null) {
      console.log('no languages gotten');
    } else {
      this.languages = data;
    }
  }

  private setError(data: any) {
    if (data === null) {
      console.log('no error message');
    } else {
      this.error = data['@out_message'];
    }
  }
}
