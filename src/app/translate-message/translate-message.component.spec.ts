import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateMessageComponent } from './translate-message.component';

describe('TranslateMessageComponent', () => {
  let component: TranslateMessageComponent;
  let fixture: ComponentFixture<TranslateMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TranslateMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranslateMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
