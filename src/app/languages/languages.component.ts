import { Component, OnInit } from '@angular/core';
import { LanguagesService } from '../languages.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

export interface Languages {
  idLanguage: string;
  dtLanguageName: string;
}

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.sass']
})
export class LanguagesComponent implements OnInit {

  public languages: Languages[] = [];
  public error: string;

  public displayedColumns: string[] = ['idLanguage', 'dtLanguageName', 'button'];

  constructor(private cookie: CookieService, private router: Router, private languagesService: LanguagesService) {
  }

  public createLanguageForm: FormGroup = new FormGroup({
    languageId: new FormControl('', [Validators.required, Validators.maxLength(2)]),
    languageName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
  });

  ngOnInit(): void {
    if (typeof (window) !== 'undefined') {
      if (this.cookie.check('token')) {
        this.getAllLanguages();
      } else {
        this.router.navigate(['']).then();
      }
    }
  }

  public getAllLanguages() {
    this.languagesService.getAllLanguages().subscribe(
      data => this.setLanguages(data),
      error => console.log(error),
    );
  }

  public createLanguage() {
    if (this.createLanguageForm.valid) {
      this.languagesService.createLanguage(this.createLanguageForm.value).subscribe(
        data => this.setError(data),
        error => console.log(error),
      );
    }
    this.getAllLanguages();
  }

  public deleteLanguage(languageId: string) {
    this.languagesService.deleteLanguage(languageId).subscribe(
      data => this.setError(data),
      error => console.log(error),
    );
    this.getAllLanguages();
  }

  private setLanguages(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.languages = data;
    }
  }

  private setError(data: any): void {
    console.log(data);
    if (data === null) {
      console.log('hey');
    } else {
      this.error = data['@out_message'];
    }
  }
}
