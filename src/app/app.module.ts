import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { MessagesComponent } from './messages/messages.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { LanguagesComponent } from './languages/languages.component';
import { MessageTypesComponent } from './message-types/message-types.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MatTabsModule } from '@angular/material/tabs';
import { CookieService } from 'ngx-cookie-service';
import { MessageDetailComponent } from './message-detail/message-detail.component';
import { SendMessageComponent } from './send-message/send-message.component';
import { JwtInterceptor } from './jwt/jwt.interceptor';
import { LanguageDetailComponent } from './language-detail/language-detail.component';
import { TranslateMessageComponent } from './translate-message/translate-message.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    MessagesComponent,
    SubscriptionsComponent,
    LanguagesComponent,
    MessageTypesComponent,
    LoginComponent,
    RegisterComponent,
    MessageDetailComponent,
    SendMessageComponent,
    LanguageDetailComponent,
    TranslateMessageComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'messages', component: MessagesComponent },
      { path: 'messages/:messageId', component: MessageDetailComponent },
      { path: 'translate', component: TranslateMessageComponent },
      { path: 'send-message', component: SendMessageComponent },
      { path: 'subscriptions', component: SubscriptionsComponent },
      { path: 'message-types', component: MessageTypesComponent },
      { path: 'languages', component: LanguagesComponent },
      { path: 'languages/:languageId', component: LanguageDetailComponent },
    ]),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatListModule,
    MatExpansionModule,
    MatTableModule,
    MatFormFieldModule,
    MatCardModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    MatTabsModule,
    FormsModule
  ],
  providers: [CookieService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
